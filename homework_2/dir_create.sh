#!/usr/bin/env bash
ansible-playbook dir_create.yaml \
	-i inventories/dev/hosts \
	"$@"
