#!/usr/bin/env bash
ansible-playbook user_create.yaml \
	-i inventories/dev/hosts \
	"$@"
