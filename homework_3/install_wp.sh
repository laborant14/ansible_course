#!/usr/bin/env bash
ansible-galaxy install \
	-r roles_requirements.yml
ansible-playbook \
	-i inventories/dev/hosts wordpress.yml -b \
	"$@"
